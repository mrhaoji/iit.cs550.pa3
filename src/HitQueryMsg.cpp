/*
 * HitQueryMsg.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "HitQueryMsg.h"
#include "Message.h"

#include "Const-impl.h"

#include<stdio.h>
#include<string.h>

HitQueryMsg::HitQueryMsg() :
		_hqMap() {

}

HitQueryMsg::HitQueryMsg(const MsgId &msgId, int ttl, const string filename) :
		Message(msgId, ttl, filename), _hqMap() {
}

HitQueryMsg::~HitQueryMsg() {
}

string HitQueryMsg::toString() const {

	return Message::toString();
}

HitQueryMsg& HitQueryMsg::assign(string sHitQueryMsg) {

	return *this;
}

//string HitQueryMsg::getFormat() {
//
//	return "%s,%s";
//}
