/*
 * Address.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef ADDRESS_H_
#define ADDRESS_H_

#include <string>
using namespace std;

/*
 *
 */
class Address {
public:
	Address();
	Address(const string& saddress);
	Address(const string &host, const string &port);
	virtual ~Address();

	string host();
	void host(const string &host);
	string port();
	void port(const string &port);

	string toString() const;
	Address& assign(string saddress);
	static string getFormat();

private:
	string _host;
	string _port;
};

#endif /* ADDRESS_H_ */
