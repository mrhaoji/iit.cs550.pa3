/*
 * QueryMsg.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "QueryMsg.h"
#include "Message.h"

#include "Const-impl.h"

#include <stdio.h>
#include <string.h>

QueryMsg::QueryMsg() {

}

QueryMsg::QueryMsg(const MsgId &msgId, int ttl, const string filename) :
		Message(msgId, ttl, filename) {
}

QueryMsg::~QueryMsg() {
}


string QueryMsg::toString() const {

	return Message::toString();
}

QueryMsg& QueryMsg::assign(string sQueryMsg) {

	return *this;
}

//string QueryMsg::getFormat() {
//
//	return "%s,%s";
//}
