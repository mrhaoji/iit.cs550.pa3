/*
 * MsgId.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "MsgId.h"
#include "PeerId.h"

#include "Const-impl.h"

#include <stdio.h>
#include <string.h>

using namespace iit::cs550::pa;

MsgId::MsgId() :
		_seqNum(""), _peerId() {

}

MsgId::MsgId(const string &seqNum, const PeerId &peerId) :
		_seqNum(seqNum), _peerId(peerId) {

}

MsgId::~MsgId() {
}

string MsgId::seqNum() {

	return _seqNum;
}

void MsgId::seqNum(const string& seqNum) {

	_seqNum = seqNum;
}

PeerId MsgId::peerId() {

	return _peerId;
}

void MsgId::peerId(const PeerId& peerId) {

	_peerId = peerId;
}

string MsgId::toString() const {

	char buf[50];
	memset(buf, 0, sizeof(buf));
	int n = sprintf(buf, getFormat().c_str(), _seqNum.c_str(), _peerId.toString().c_str());

	string result(buf, 0, n);

	return result;
}

MsgId& MsgId::assign(string sMsgId) {

	const char* delimiter = ",";

	string remains = Const::trim(sMsgId);

	size_t found = remains.find(delimiter);

	if (found != string::npos) {

		seqNum(Const::trim(remains.substr(0, int(found))));

		PeerId peerId_MsgId;
		peerId_MsgId.assign(Const::trim(remains.substr(0, int(found))));
		peerId(peerId_MsgId);
	}

	return *this;
}

string MsgId::getFormat() {

	return "%s,%d";
}
