/*
 * InvalidationMsg.cpp
 *
 *  Created on: Oct 24, 2013
 *      Author: parallels
 */

#include "InvalidationMsg.h"

#include "Const-impl.h"

#include <stdio.h>
#include <string.h>

InvalidationMsg::InvalidationMsg() : _msgId(), _filename(""), _versionNum(0) {

}

InvalidationMsg::InvalidationMsg(const MsgId &msgId, const string filename, int versionNum) :
		_msgId(msgId), _ttl(ttl), _filename(filename) {
}

InvalidationMsg::~InvalidationMsg() {
}

MsgId InvalidationMsg::msgId() {

	return _msgId;
}
void InvalidationMsg::msgId(const MsgId& msgId) {

	_msgId = msgId;
}

int InvalidationMsg::versionNum() {

	return _versionnum;
}

void InvalidationMsg::versionNum(const int& versionnum) {

	_versionnum = versionnum;
}

string Message::filename() {

	return _filename;
}

void Message::filename(const string& filename) {

	_filename = filename;
}

string InvalidationMsg::toString() const {

	char buf[50];
	memset(buf, 0, sizeof(buf));
	int n = sprintf(buf, getFormat().c_str(), _msgId.toString().c_str(), _filename.c_str(), _versionnum);

	string result(buf, 0, n);

	return result;
}

InvalidationMsg& InvalidationMsg::assign(string sInvalidationMsg) {

	StrTokenizer strtok(sMessage, ",");

	_msgId.assign(strtok.next_token());
	_filename = strtok.next_token();
	_versionnum = atoi(strtok.next_token().c_str());

	return *this;
}

string InvalidationMsg::getFormat() {

	return "%d,%s,%d";
}
