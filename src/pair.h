/*
 * pair.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef PAIR_H_
#define PAIR_H_

#include "MsgId.h"
#include "PeerId.h"

class Pair {
public:
	Pair();
	Pair(const MsgId &msgId);
	virtual ~Pair();

//	MsgId get_msgId();

	MsgId msgId();
	void msgId(const MsgId& msgId);

	virtual string toString() const;
	virtual Pair& assign(string sPair);

private:
	MsgId _msgId;
};

/*
 *
 */
class HitQueryPair: public Pair {
public:
	HitQueryPair();
	HitQueryPair(const MsgId &msgId, const PeerId &peerId);
	virtual ~HitQueryPair();

//	PeerId get_peerId();

	PeerId peerId();
	void peerId(const PeerId& peerId);

	string toString() const;
	HitQueryPair& assign(string sHitQueryPair);

private:
	PeerId _peerId;
};

/*
 *
 */
class SeenMsgPair: public Pair {
public:
	SeenMsgPair();
	SeenMsgPair(const MsgId &msgId);
	virtual ~SeenMsgPair();
};

#endif /* PAIR_H_ */
