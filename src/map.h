/*
 * map.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef MAP_H_
#define MAP_H_

#include <string>
#include <map>
using namespace std;

#include  "pair.h"
#include  "Address.h"

/*
 *
 */
class HitQueryMap: map<string, HitQueryPair> {
public:
	HitQueryMap();
	virtual ~HitQueryMap();
};

class SeenMsgMap: map<string, SeenMsgPair> {
public:
	SeenMsgMap();
	virtual ~SeenMsgMap();
};

class AddressMap: map<string, Address> {
public:
	AddressMap();
	virtual ~AddressMap();
};

#endif /* MAP_H_ */
