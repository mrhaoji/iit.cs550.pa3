/*
 * pair.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "pair.h"
#include "MsgId.h"
#include "Message.h"

#include "Const-impl.h"

#include<stdio.h>
#include<string.h>

using namespace iit::cs550::pa;

Pair::Pair() {
}

Pair::Pair(const MsgId &msgId) :
		_msgId(msgId) {
}

MsgId Pair::msgId() {

	return _msgId;
}

void Pair::msgId(const MsgId& msgId) {

	_msgId = msgId;
}

string Pair::toString() const {

	return _msgId.toString().c_str();
}

Pair& Pair::assign(string sPair) {

	_msgId.assign(sPair);

	return *this;
}

Pair::~Pair() {
}

HitQueryPair::HitQueryPair() {
}

HitQueryPair::HitQueryPair(const MsgId &msgId, const PeerId &peerId) :
		Pair(msgId), _peerId(peerId) {
}

HitQueryPair::~HitQueryPair() {
}

PeerId HitQueryPair::peerId() {

	return _peerId;
}

void HitQueryPair::peerId(const PeerId& peerId) {

	_peerId = peerId;
}

string HitQueryPair::toString() const {

	return _peerId.toString().c_str();
}

HitQueryPair& HitQueryPair::assign(string sHitQueryPair) {

	_peerId.assign(sHitQueryPair);

	return *this;
}

SeenMsgPair::SeenMsgPair() {
}

SeenMsgPair::SeenMsgPair(const MsgId &msgId) :
		Pair(msgId) {
}

SeenMsgPair::~SeenMsgPair() {
}
