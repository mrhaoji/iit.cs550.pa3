/*
 * PeerId.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef PEERID_H_
#define PEERID_H_

#include <string>
using namespace std;
/*
 *
 */
class PeerId {
public:
	PeerId();
	PeerId(const string &peerIp, const string &peerPort);
	virtual ~PeerId();

//	string get_peerIp();
//	string get_peerPort();

	string peerIp();
	void peerIp(const string& peerIp);

	string peerPort();
	void peerPort(const string& peerPort);

	string toString() const;
	PeerId& assign(string sPeerId);

	static string getFormat();

private:
	string _peerIp;
	string _peerPort;
};

#endif /* PEERID_H_ */
