/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * cpp_zhtclient_test.cpp
 *
 *  Created on: Aug 7, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#include "PeerClient.h"

#include "zpack.pb.h"

#include  <getopt.h>
#include  <stdlib.h>
#include   <stdio.h>
#include   <string>
#include   <exception>
using namespace std;

using namespace iit::cs550::pa;

typedef vector<string> VEC;
typedef vector<string>::const_iterator IT;

void printUsage(char *argv_0);
void test_dispatch();

PeerClient pc;
int lookupfl = 0, downloadfl = 0, uploadfl = 0, removefl = 0;
string envConf = "";
string neighborconf = "";
string filename = "";
string filename_down = "";
string peeraddress = "";

int main(int argc, char **argv) {

	extern char *optarg;

	int printHelp = 0;

	int c;
	while ((c = getopt(argc, argv, "c:n:t:f:d:lurh")) != -1) {
		switch (c) {
		case 'c':
			envConf = string(optarg);
			break;
		case 'n':
			neighborconf = string(optarg);
			break;
		case 'f':
			filename = string(optarg);
			break;
		case 't':
			peeraddress = string(optarg);
			break;
		case 'l':
			lookupfl = 1;
			break;
		case 'd':
			downloadfl = 1;
			filename_down = string(optarg);
			break;
		case 'u':
			uploadfl = 1;
			break;
		case 'r':
			removefl = 1;
			break;
		case 'h':
			printHelp = 1;
			break;
		default:
			fprintf(stderr, "Illegal argument \"%c\"\n", c);
			printUsage(argv[0]);
			exit(1);
		}
	}

	int helpPrinted = 0;
	if (printHelp) {
		printUsage(argv[0]);
		helpPrinted = 1;
	}

	try {

		if (!envConf.empty() && !filename.empty()) {

			test_dispatch();

		} else {

			if (!helpPrinted)
				printUsage(argv[0]);
		}
	} catch (exception& e) {

		fprintf(stderr, "%s, exception caught:\n\t%s", "ZHTServer::main",
				e.what());
	}

}

void test_lookup() {

	if (!lookupfl)
		return;

	VEC peerlist;
	int status = pc.lookup_file(peeraddress, filename, peerlist);

	int size = peerlist.size();

	if (size > 0) {

		for (int i = 0; i < size; i++) {

			ZPack prpack;
			prpack.ParseFromString(peerlist[i]);

			fprintf(stderr, "file[%s] exists in peer<%s:%u>\n",
					filename.c_str(), prpack.prhost().c_str(), prpack.prport());
		}
	} else {

		fprintf(stderr, "file[%s] not found in any peer\n", filename.c_str());
	}
}

void test_download() {

	if (!downloadfl)
		return;

	int status = pc.obtain_file(peeraddress, filename, filename_down);

	if (status == 0)
		fprintf(stderr, "rc[%d] file[%s] downloaded to file[%s]\n", status,
				filename.c_str(), filename_down.c_str());
	else
		fprintf(stderr, "rc[%d] file[%s] not downloaded to file[%s]\n", status,
				filename.c_str(), filename_down.c_str());

}

void test_upload() {

	if (!uploadfl)
		return;

	int status = pc.upload_file(filename);

	if (status == 0)
		fprintf(stderr, "rc[%d] file[%s] uploaded\n", status, filename.c_str());
	else
		fprintf(stderr, "rc[%d] file[%s] not uploaded\n", status,
				filename.c_str());
}

void test_remove() {

	if (!removefl)
		return;

	int status = pc.delete_file(filename);

	if (status == 0)
		fprintf(stderr, "rc[%d] file[%s] deleted\n", status, filename.c_str());
	else
		fprintf(stderr, "rc[%d] file[%s] not deleted\n", status,
				filename.c_str());
}

void test_dispatch() {

	pc.init(envConf, neighborconf);

	test_lookup();

	test_download();

	test_upload();

	test_remove();

	pc.teardown();
}

void printUsage(char *argv_0) {

	fprintf(stdout, "Usage:\n%s %s\n", argv_0,
			"-c env.conf -n neighbor.conf -f filename_searched_on_peers {-t peer_address | -l(lookup) | -d filename_down_to_local | -u(upload) | -r(remove)} | [-h(help)]");
}
