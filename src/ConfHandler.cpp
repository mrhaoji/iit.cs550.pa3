/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * ConfHandler.cpp
 *
 *  Created on: Aug 7, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#include "ConfHandler.h"
#include "ConfEntry.h"
#include "StrTokenizer.h"
#include "Address.h"

#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdio.h>

#include <fstream>
#include <iostream>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

bool ConfHandler::BEEN_INIT = false;
string ConfHandler::CONF_ENV = "env.conf";
string ConfHandler::CONF_NEIGHBOR = "neighbor_mesh.conf";

ConfHandler::MAP ConfHandler::EnvParameters = MAP();
ConfHandler::MAP ConfHandler::NeighborParameters = MAP();
ConfHandler::NMAP ConfHandler::NeighborMap = NMAP();
ConfHandler::VEC ConfHandler::NeighborVec = VEC();

ConfHandler::ConfHandler() {

}

ConfHandler::~ConfHandler() {

}

string ConfHandler::getProtocolFromConf() {

	ConfHandler::MAP *zpmap = &ConfHandler::EnvParameters;

	ConfHandler::MIT it;

	for (it = zpmap->begin(); it != zpmap->end(); it++) {

		ConfEntry ce;
		ce.assign(it->first);

		if (ce.name() == Const::PROTO_NAME) {

			return ce.value();
		}
	}

	return "";
}

string ConfHandler::getPortFromConf() {

	ConfHandler::MAP *zpmap = &ConfHandler::EnvParameters;

	ConfHandler::MIT it;

	for (it = zpmap->begin(); it != zpmap->end(); it++) {

		ConfEntry ce;
		ce.assign(it->first);

		if (ce.name() == Const::PROTO_PORT) {

			return ce.value();
		}
	}

	return "";
}

string ConfHandler::getIdxSrv() {

	ConfHandler::MAP *zpmap = &ConfHandler::EnvParameters;

	ConfHandler::MIT it;

	for (it = zpmap->begin(); it != zpmap->end(); it++) {

		ConfEntry ce;
		ce.assign(it->first);

		if (ce.name() == Const::IDX_SRV) {

			return ce.value();
		}
	}

	return "";
}

uint ConfHandler::getIdxPort() {

	return atoi(getIdxPort_str().c_str());
}

string ConfHandler::getIdxPort_str() {

	ConfHandler::MAP *zpmap = &ConfHandler::EnvParameters;

	ConfHandler::MIT it;

	for (it = zpmap->begin(); it != zpmap->end(); it++) {

		ConfEntry ce;
		ce.assign(it->first);

		if (ce.name() == Const::IDX_PRT) {

			return ce.value();
		}
	}

	return "";
}

void ConfHandler::initConf(string envConf) {

	if (!BEEN_INIT) {

		ConfHandler::CONF_ENV = envConf; //env.conf
		ConfHandler::setEnvParameters(envConf);

		BEEN_INIT = true;
	}
}

void ConfHandler::clear_all() {

	EnvParameters.clear();
	NeighborParameters.clear();
	NeighborMap.clear();
	NeighborVec.clear();
}

void ConfHandler::initConf(string envConf, string neighborconf) {

	if (!BEEN_INIT) {

		ConfHandler::CONF_ENV = envConf; //env.conf
		ConfHandler::CONF_NEIGHBOR = neighborconf; //neighbor.conf

		ConfHandler::setEnvParameters(envConf);
		ConfHandler::setNeighborParameters(neighborconf);

		BEEN_INIT = true;
	}
}

void ConfHandler::setEnvParameters(const string& zhtConfig) {

	setParametersInternal(zhtConfig, EnvParameters);

	pickEnvParameters();
}

void ConfHandler::setNeighborParameters(const string& neighborconf) {

	setParametersInternal(neighborconf, NeighborParameters);

	pickNeighborParameters();
}

void ConfHandler::setParametersInternal(string configFile, MAP& configMap) {

	ifstream ifs(configFile.c_str(), ifstream::in);

	const char *delimiter = Const::CONF_DELIMITERS.c_str();

	string line;
	while (getline(ifs, line)) {

		string remains = line;

		if (remains.empty())
			continue;

		if (remains.substr(0, 1) == "#") //starts with #, means comment
			continue;

		StrTokenizer strtok(remains);

		string one;
		string two;

		if (strtok.has_more_tokens())
			one = strtok.next_token();

		if (strtok.has_more_tokens())
			two = strtok.next_token();

		if (one.empty())
			continue;

		ConfEntry ce(one, two);
		configMap.insert(PAIR(ce.toString(), ce)); //todo: use hash code to reduce size of key/value pair.
		//configMap[ce.toString()] = ce; //todo: use hash code to reduce size of key/value pair.
	}

	ifs.close();
}

void ConfHandler::pickEnvParameters() {

	ConfHandler::MIT it;
	ConfHandler::MAP* map = &ConfHandler::EnvParameters;

	for (it = map->begin(); it != map->end(); it++) {

		ConfEntry kv = it->second;
	}
}

void ConfHandler::print_map() {

	NMIT mit;
	for (mit = NeighborMap.begin(); mit != NeighborMap.end(); mit++) {

		fprintf(stdout, "<%s>:\n", mit->first.c_str());

		VEC neighbors = NeighborMap[mit->first];

		VIT vit;
		//for (vit = mit->second.begin(); vit != mit->second.end(); vit++) {
		for (vit = neighbors.begin(); vit != neighbors.end(); vit++) {

			fprintf(stdout, "\t%s\n", vit->toString().c_str());
		}

		fprintf(stdout, "\n");
	}

	fflush(stdout);
}

void ConfHandler::pickNeighborParameters() {

	ConfHandler::MIT it;
	ConfHandler::MAP* map = &ConfHandler::NeighborParameters;

	for (it = map->begin(); it != map->end(); it++) {

		//string key = it->first; //localhost:50010,localhost:50005;localhost:50009
		ConfEntry ce = it->second; //ConfEntry(localhost:50010,localhost:50005;localhost:50009)

		Address addr_key(ce.name());
		ConfEntry ce_key(addr_key.host(), addr_key.port());
		if (NeighborMap.find(ce_key.toString()) == NeighborMap.end()) {
			NeighborMap[ce_key.toString()] = VEC(); //ce.name(): localhost:50010
		}

		/*put one peer's all neighbors into NeighborMap*/
		string value = ce.value();

		char * pch, *sp;
		pch = strtok_r((char*) value.c_str(), ";", &sp);
		while (pch != NULL) {

			Address addr_ele(pch);
			ConfEntry ce_ele(addr_ele.host(), addr_ele.port()); //localhost,50005 OR localhost,50009
			NeighborMap[ce_key.toString()].push_back(ce_ele);

			pch = strtok_r(NULL, ";", &sp);
		}

		//print_map();

		/*put all peers into NeighborVec*/
		NeighborVec.push_back(ce_key); //ce.name(): localhost:50010
	}
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
