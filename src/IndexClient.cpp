/*
 * IndexClient.cpp
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#include "IndexClient.h"
#include "zpack.pb.h"
#include "Const.h"
#include "ConfHandler.h"
#include "Env.h"

namespace iit {
namespace cs550 {
namespace pa {

IndexClient::IndexClient() :
		_proxy(0), _msg_maxsize(0) {
}

IndexClient::~IndexClient() {

	if (_proxy != NULL) {

		delete _proxy;
		_proxy = NULL;
	}
}

int IndexClient::init(const string &envConf) {

	ConfHandler::initConf(envConf);

	_msg_maxsize = Env::get_msg_maxsize();

	_proxy = ProxyStubFactory::createProxy();

	if (_proxy == 0)
		return -1;
	else
		return 0;
}

string IndexClient::sendrecv(const string &msg, string &result) {

	char *buf = (char*) calloc(_msg_maxsize, sizeof(char));
	size_t msz = _msg_maxsize;

	/*send to and receive from*/
	_proxy->sendrecv(msg.c_str(), msg.size(), buf, msz);

	/*...parse status and result*/
	string sstatus;

	string srecv(buf);

	if (srecv.empty()) {

		sstatus = Const::PSC_REC_SRVEXP;
	} else {

		result = srecv.substr(3); //the left, if any, is lookup result or second-try zpack
		sstatus = srecv.substr(0, 3); //status returned, the first three chars, like 001, -98...
	}

	free(buf);

	return sstatus;
}

int IndexClient::shared_op(const VEC &filelist, const string &op,
		string &result) {

	return shared_op("", filelist, op, result);
}

int IndexClient::shared_op(const string &peer, const VEC &filelist,
		const string &op, string &result) {

	ZPack zpack;

	zpack.set_opcode(op);
	if (!peer.empty()) {

		ZPack prpack;
		prpack.ParseFromString(peer);

		zpack.set_peerid(prpack.peerid());
		zpack.set_prhost(prpack.host());
		zpack.set_prport(prpack.port());
	}

	zpack.set_host(ConfHandler::getIdxSrv());
	zpack.set_port(ConfHandler::getIdxPort());

	for (IT it = filelist.begin(); it != filelist.end(); ++it) {

		zpack.add_filelist(*it);
	}

	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	int status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	return status;
}

/*
 * peer: zpack with peerid, host, port
 *
 * */
int IndexClient::register_peer(const string &peer, const VEC &filelist) {

	string result;
	return shared_op(peer, filelist, Const::PSC_OPC_REGPR, result);
}

int IndexClient::unregister_peer(const string &peer) {

	VEC filelist;
	string result;

	return shared_op(peer, filelist, Const::PSC_OPC_UNREGPR, result);
}

/*
 * peerlist:
 * 	every one is ZPack: peerid, host, port
 *
 */
int IndexClient::search_file(const string &filename, VEC &peerlist) {

	string result;

	VEC filelist;
	filelist.push_back(filename);

	int status = shared_op(filelist, Const::PSC_OPC_SRCHFL, result);

	//extract peers that own the specified file with filename
	ZPack zpack;
	zpack.ParseFromString(result);

	int size = zpack.peerlist().size();
	for (int i = 0; i < size; i++) {

		peerlist.push_back(zpack.peerlist(i));
	}

	return status;
}

int IndexClient::add_file(const string &peer, const VEC &filelist) {

	string result;
	return shared_op(peer, filelist, Const::PSC_OPC_ADDFL, result);
}

int IndexClient::delete_file(const string &peer, const VEC &filelist) {

	string result;
	return shared_op(peer, filelist, Const::PSC_OPC_DELFL, result);
}

int IndexClient::teardown() {

	if (_proxy->teardown())
		return 0;
	else
		return -1;
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
