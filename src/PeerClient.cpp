/*
 * PeerClient.cpp
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#include "PeerClient.h"
#include "ConfHandler.h"
#include "ConfEntry.h"
#include "Env.h"
#include "StrTokenizer.h"
#include "zpack.pb.h"
#include "IndexClient.h"
#include "Const-impl.h"
#include "ZHTUtil.h"
#include "Util.h"
#include "Address.h"

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

namespace iit {
namespace cs550 {
namespace pa {

PeerClient::PeerClient() :
		_proxy(0), _msg_maxsize(0) {
}

PeerClient::~PeerClient() {

	if (_proxy != NULL) {

		delete _proxy;
		_proxy = NULL;
	}
}

int PeerClient::init(const string &envConf) {

	this->envConf = envConf;

	ConfHandler::initConf(envConf);

	_msg_maxsize = Env::get_msg_maxsize();

	_proxy = ProxyStubFactory::createProxy();

	if (_proxy == 0)
		return -1;
	else
		return 0;
}

int PeerClient::init(const string &envConf, const string &neighborConf) {

	this->envConf = envConf;
	this->neighborConf = neighborConf;

	ConfHandler::initConf(envConf, neighborConf);

	_msg_maxsize = Env::get_msg_maxsize();

	_proxy = ProxyStubFactory::createProxy();

	if (_proxy == 0)
		return -1;
	else
		return 0;

}

int PeerClient::get_allpeers(VEC &peerlist) {

	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_GETPRS);
	zpack.set_host(ConfHandler::getIdxSrv());
	zpack.set_port(ConfHandler::getIdxPort());

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	int status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	if (status == 0) {

		/*extract peers to peerlist*/
		ZPack zpack2;
		zpack2.ParseFromString(result);
		for (int i = 0; i < zpack2.peerlist().size(); i++) {

			/*zpack2.peerlist(i): ZPack with peerid, host and port assigned*/
			peerlist.push_back(zpack2.peerlist(i));
		}
	}

	return status;
}

string PeerClient::sendrecv(const string &msg, string &result) {

	char *buf = (char*) calloc(_msg_maxsize, sizeof(char));
	size_t msz = _msg_maxsize;

	/*send to and receive from*/
	_proxy->sendrecv(msg.c_str(), msg.size(), buf, msz);

	/*...parse status and result*/
	string sstatus;

	string srecv(buf);

	if (srecv.empty()) {

		sstatus = Const::PSC_REC_SRVEXP;
	} else {

		result = srecv.substr(3); //the left, if any, is lookup result or second-try zpack
		sstatus = srecv.substr(0, 3); //status returned, the first three chars, like 001, -98...
	}

	free(buf);

	return sstatus;
}

//todo: this may not work correctly
string PeerClient::remove_path(const string &filename) {

	StrTokenizer strtok(filename, "/");

	string result;
	while (strtok.has_more_tokens()) {

		result = strtok.next_token();
	}

	return result;
}

int PeerClient::fwd_lookup(const string &addr, const ZPack& zpack,
		VEC &peerlist) {

	ConfEntry ce(addr);

	ZPack fwdpack;
	fwdpack.ParseFromString(zpack.SerializeAsString());

	fwdpack.set_opcode(Const::PSC_OPC_FWDQUERY);
	fwdpack.set_host(ce.name());
	fwdpack.set_port(atoi(ce.value().c_str()));
	fwdpack.set_ttl(fwdpack.ttl() - 1);

	string result;
	string sstatus = sendrecv(fwdpack.SerializeAsString(), result);

	int status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	//extract peers that own the specified file with filename
	ZPack prpack;
	prpack.ParseFromString(result);

	int size = prpack.peerlist().size();
	for (int i = 0; i < size; i++) {

		//prpack.peerlist(i) is a ZPack that represents a peer
		peerlist.push_back(prpack.peerlist(i));
	}

	return status;

}

/*
 * peerlist:
 * 	every one is ZPack: peerid, host, port
 *
 */
int PeerClient::lookup_file(const string &filename, VEC &peerlist) {

	return lookup_file("", filename, peerlist);
}

//peeraddr: localhost:50001
int PeerClient::lookup_file(const string &peeraddr, const string &filename,
		VEC &peerlist) {

	//string short_filename = remove_path(filename);

	/*get peer'address*/
	string addr;
	!peeraddr.empty() ? addr = peeraddr : addr = get_random_peer_addr();
	//Address address(addr);

	ConfEntry ce(get_peer_for_file(filename));
	Address address(ce.name(), ce.value());

	/*lookup file*/
	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_SRCHFL);
	zpack.set_host(address.host());
	zpack.set_port(atoi(address.port().c_str()));
	zpack.add_filelist(filename);
	zpack.set_ttl(5);
	zpack.set_msgid(Const::toString(IdHelper::genId()));

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	int status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	//extract peers that own the specified file with filename
	ZPack prpack;
	prpack.ParseFromString(result);

	int size = prpack.peerlist().size();
	for (int i = 0; i < size; i++) {

		//prpack.peerlist(i) is a ZPack that represents a peer
		peerlist.push_back(prpack.peerlist(i));
	}

	return status;
}

int PeerClient::obtain_file(const string &filename,
		const string &filename_down) {

	return obtain_file("", filename, filename_down);
}

int PeerClient::obtain_file(const string &peeraddr, const string &filename,
		const string &filename_down) {

	int status;
	//string short_filename = remove_path(filename);

	/*get peer'address*/
	string addr;
	!peeraddr.empty() ? addr = peeraddr : addr = get_random_peer_addr();

	/*lookup file*/
	VEC peerlist;
	status = lookup_file(addr, filename, peerlist);
	if (status != 0)
		return status;

	/*no peers found in the toponogy used*/
	if (peerlist.size() == 0) {

		fprintf(stderr, "file[%s] not found in any peer\n", filename.c_str());
		return Const::PSI_REC_FLNOTFND;
	}

	/*randomly select a peer to download file*/
	srand(clock());
	int idx = rand() % peerlist.size();

	ZPack prpack;
	prpack.ParseFromString(peerlist[idx]);

	/*download file*/
	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_OBTFL);
	zpack.set_host(prpack.prhost());
	zpack.set_port(prpack.prport());
	zpack.add_filelist(filename);

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	if (status == 0) {

		/*save files to disk..., zpack.filecontent*/
		FILE *pFile = fopen(filename_down.c_str(), "w+b");

		if (pFile == NULL) {

			fprintf(stderr, "file[%s] open error\n", filename_down.c_str());
			return Const::PSI_REC_FLOPFAIL;
		}

		ftruncate(fileno(pFile), 0);
		rewind(pFile);

		ZPack rltzpack;
		rltzpack.ParseFromString(result);

		size_t size = fwrite(rltzpack.filecontent().c_str(), 1,
				rltzpack.filecontent().size(), pFile);

		if (size == rltzpack.filecontent().size())
			fprintf(stderr,
					"file[%s] downloaded to file[%s] from peer<%s:%u>\n",
					filename.c_str(), filename_down.c_str(),
					prpack.prhost().c_str(), prpack.prport());

		fclose(pFile);
	}

	return status;
}

string PeerClient::get_random_peer_addr() {

	int size = ConfHandler::NeighborVec.size();

	srand(time(NULL));
	int idx = (rand()) % size;

	//localhost:50010
	ConfEntry ce = ConfHandler::NeighborVec[idx];

	Address addr(ce.name(), ce.value());

	printf("try to search file from peer<%s,%s>\n", addr.host().c_str(),
			addr.port().c_str());

	return addr.toString();
}

int PeerClient::uploadfile_internal(const string &host, const uint &port,
		const string &filename, const char *filecontent) {

	/*be going to upload file*/
	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_UPLFL);
	zpack.set_host(host); //todo: set peer host/port
	zpack.set_port(port);
	zpack.add_filelist(filename);
	zpack.set_filecontent(filecontent);
	zpack.set_versionnum(1);

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	int status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	if (status == 0) {

		fprintf(stderr, "file[%s] uploaded to peer<%s:%u>\n", filename.c_str(),
				host.c_str(), port);
	} else {

		fprintf(stderr, "file[%s] not uploaded to peer<%s:%u>\n",
				filename.c_str(), host.c_str(), port);
	}

	return status;
}

//localhost:50010
string PeerClient::get_peer_for_file(const string &filename) {

	uint64_t hashcode = HashUtil::genHash(filename.c_str());

	int size = ConfHandler::NeighborVec.size();

	int idx = hashcode % size;

	ConfEntry ce = ConfHandler::NeighborVec[idx];

	return ce.toString();

}

int PeerClient::upload_file(const string &filename) {

	int status;
	//string short_filename = remove_path(filename);

	/*read file from disk..., assign to zpack.filecontent*/
	FILE * pFile;
	pFile = fopen(filename.c_str(), "r+b");
	if (pFile == NULL) {

		fprintf(stderr, "file[%s] open error\n", filename.c_str());
		return Const::PSI_REC_FLOPFAIL;
	}

	fseek(pFile, 0, SEEK_END);
	size_t size = ftell(pFile);
	rewind(pFile);
	char buf[size];
	fread(buf, 1, size, pFile);
	fclose(pFile);

	string peeraddr = get_peer_for_file(filename);
	ConfEntry ce(peeraddr);

	status = uploadfile_internal(ce.name(), atoi(ce.value().c_str()), filename,
			buf);

	return status;
}

int PeerClient::delete_file(const string &filename) {

	int status;
	//string short_filename = remove_path(filename);

	string peeraddr = get_peer_for_file(filename);
	ConfEntry ce(peeraddr);

	/*be going to delete file from one peer*/
	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_DELFL);
	zpack.set_host(ce.name()); //todo: set peer host/port
	zpack.set_port(atoi(ce.value().c_str()));
	zpack.add_filelist(filename);

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);

	status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	if (status == 0) {

		fprintf(stderr, "file[%s] deleted from peer<%s:%u>\n", filename.c_str(),
				zpack.host().c_str(), zpack.port());
	} else {

		fprintf(stderr, "file[%s] not deleted from peer<%s:%u>\n",
				filename.c_str(), zpack.host().c_str(), zpack.port());
	}

	return status;
}

int PeerClient::teardown() {

	if (_proxy->teardown())
		return 0;
	else
		return -1;
}

int PeerClient::invalidate_file(const string &filename) {

	int status;

	string peeraddr = get_peer_for_file(filename);
	ConfEntry ce(peeraddr);

	ZPack zpack;
	zpack.set_opcode(Const::PSC_OPC_INVFL);
	zpack.set_host(ce.name()); //todo: set peer host/port
	zpack.set_port(atoi(ce.value().c_str()));
	zpack.add_filelist(filename);

	VEC peerlist;
	this->lookup_file(filename, peerlist);

	ZPack prpack;
	prpack.ParseFromString(peerlist[0]);
	prpack.host(); //file peer add

	string result;
	string sstatus = sendrecv(zpack.SerializeAsString(), result);
	string filename_down = "";

	status = Const::PSI_REC_CLTFAIL;
	if (!sstatus.empty())
		status = Const::toInt(sstatus);

	if (status == 0) {

		/*save files to disk..., zpack.filecontent*/
		FILE *pFile = fopen(filename_down.c_str(), "w+b");

		if (pFile == NULL) {

			fprintf(stderr, "file[%s] open error\n", filename_down.c_str());
			return Const::PSI_REC_FLOPFAIL;
		}

	ftruncate(fileno(pFile), 0);
	rewind(pFile);

	ZPack rltzpack;
	rltzpack.ParseFromString(result);

	rltzpack.versionnum();
	rltzpack.set_versionnum(rltzpack.versionnum()+1);
	rltzpack.set_host(ce.name());
	rltzpack.set_port(atoi(ce.value().c_str()));

	string sfresult;
	string sfstatus = sendrecv(rltzpack.SerializeAsString(), sfresult);

	fclose(pFile);

	}

	return status;
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
