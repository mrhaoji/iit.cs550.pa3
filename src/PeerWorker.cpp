/*
 * PeerWorker.cpp
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#include "PeerWorker.h"
#include "Const.h"
#include "IndexClient.h"
#include "ConfHandler.h"
#include "Address.h"

#include "PeerClient.h"

#include <iostream>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

string PeerWorker::HOST = "localhost";
string PeerWorker::PORT = "";

//NoVoHT* PeerWorker::pmap = new NoVoHT("files.index.store", 100000, 10000, 0.7);
NoVoHT* PeerWorker::pmap = new NoVoHT("", 100000, 10000, 0.7);

PeerWorker::MAP PeerWorker::SMM = PeerWorker::MAP();
PeerWorker::MAP PeerWorker::FMM = PeerWorker::MAP();

PeerWorker::PeerWorker() {
}

PeerWorker::~PeerWorker() {
}

string PeerWorker::run(const char *buf) {

	string result;

	ZPack zpack;
	string str(buf);
	zpack.ParseFromString(str);

	if (zpack.opcode() == Const::PSC_OPC_OBTFL) {

		result = obtain_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_UPLFL) {

		result = upload_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_DELFL) {

		result = delete_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_SRCHFL) {

		result = search_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_FWDQUERY) {

		result = fwd_query(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_INVFL){

		result = invalidate_file(zpack);
	} else {
		result = Const::PSC_REC_UOPC;
	}

	return result;
}

string PeerWorker::obtain_file(const ZPack &zpack) {

	string result;

	string key = zpack.filelist(0);
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	/*lookup files in that peer*/
	string *lkpresult = pmap->get(key);
	if (lkpresult == NULL) {

		cerr << "DB Error: lookup find nothing" << endl;

		result = Const::PSC_REC_NONEXISTKEY;

		return result;
	}

	ZPack zpack2;
	zpack2.ParseFromString(*lkpresult);

	result = Const::PSC_REC_SUCC;
	result.append(zpack2.SerializeAsString());

	return result;
}

string PeerWorker::upload_file(const ZPack &zpack) {

	string result;

	string key = zpack.filelist(0);
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	int ret = pmap->put(key, zpack.SerializeAsString());

	if (ret != 0) {

		cerr << "DB Error: fail to insert: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

string PeerWorker::delete_file(const ZPack &zpack) {

	string result;

	string key = zpack.filelist(0);
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	int ret = pmap->remove(key);

	if (ret != 0) {

		cerr << "DB Error: fail to remove: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

string PeerWorker::fwd_query(const ZPack &zpack) {

	string result = Const::PSC_REC_SUCC;

	if (zpack.ttl() <= 0) //
		return result;

	if (saw_msgid(zpack.msgid(), FMM)) //
		return result;

	put_msgid(zpack.msgid(), FMM); //memorize message id forwarded

	result = search_file(zpack);

	del_msgid(zpack.msgid(), FMM); //delete message id to relase memory

	return result;
}

string PeerWorker::search_file(const ZPack &zpack) {

	string result = Const::PSC_REC_SUCC;

	if (zpack.ttl() <= 0) //
		return result;

	if (saw_msgid(zpack.msgid(), SMM)) //
		return result;

	put_msgid(zpack.msgid(), SMM); //memorize message id processed

	string filename_passedin = zpack.filelist(0);

	string *lkpresult = pmap->get(filename_passedin);

	if (lkpresult == NULL) {

		/*cerr << "DB Error: lookup find nothing" << endl;
		 result = Const::PSC_REC_NONEXISTKEY;*/

		//result = search_file_from_neighbors(zpack);
		del_msgid(zpack.msgid(), SMM); //delete message id to relase memory

		return result;

	} else {

		/*printf("searching <%s:%d> from <%s>\n", zpack.msgid().c_str(),
				zpack.ttl(), get_addr().c_str());*/

		/*prpack: peerid, host, port*/
		ZPack rltpack;

		ZPack prpack;
		prpack.set_prhost(PeerWorker::HOST);
		prpack.set_prport(atoi(PeerWorker::PORT.c_str()));
		rltpack.add_peerlist(prpack.SerializeAsString());

		result.append(rltpack.SerializeAsString());

		del_msgid(zpack.msgid(), SMM); //delete message id to relase memory

	}

	return result;
}

string PeerWorker::search_file_from_neighbors(const ZPack &zpack) {

	VEC rltprlist;

	ConfHandler::VEC *neighbors = &ConfHandler::NeighborMap[get_addr()];

	PeerClient pc;
	pc.init(ConfHandler::CONF_ENV, ConfHandler::CONF_NEIGHBOR);

	/*search all neighbors by forwarding query*/
	ConfHandler::VIT it;
	for (it = neighbors->begin(); it != neighbors->end(); it++) { //it: ConfEntry

		VEC prlist;
		put_msgid(zpack.msgid(), FMM);

		/*printf("forwarding <%s:%d> from <%s> to <%s>\n", zpack.msgid().c_str(),
		 zpack.ttl(), get_addr().c_str(), it->toString().c_str());
*/
		pc.fwd_lookup(it->toString(), zpack, prlist);

		int i;
		for (i = 0; i < prlist.size(); i++) {

			rltprlist.push_back(prlist[i]);
			break;
		}
	}
	pc.teardown();

	/*extract all peerlist that contains the file*/
	ZPack rltpack;
	int i;
	for (i = 0; i < rltprlist.size(); i++) {

		rltpack.add_peerlist(rltprlist[i]);
	}

	string result = Const::PSC_REC_SUCC;
	result.append(rltpack.SerializeAsString());

	return result;
}

string PeerWorker::get_addr() {

	ConfEntry ce(HOST, PORT);

	return ce.toString();

}

void PeerWorker::put_msgid(const string &msgid, MAP &map) {

	map[msgid] = msgid;
}

void PeerWorker::del_msgid(const string &msgid, MAP &map) {

	MIT it = map.find(msgid);
	if (it != map.end()) {

		map.erase(it);
	}
}

bool PeerWorker::saw_msgid(const string &msgid, MAP &map) {

	bool result = false;

	MIT it = map.find(msgid);
	if (it != map.end()) {

		result = true;
	}

	return result;
}

string PeerWorker::invalidate_file(const ZPack &zpack) {

	string result;

	result = upload_file(zpack);

	return result;
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
