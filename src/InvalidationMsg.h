/*
 * InvalidationMsg.h
 *
 *  Created on: Oct 24, 2013
 *      Author: parallels
 */

#ifndef INVALIDATIONMSG_H_
#define INVALIDATIONMSG_H_

#include <string>
using namespace std;
#include "MsgId.h"

class InvalidationMsg {
public:
	InvalidationMsg();
	InvalidationMsg(const MsgId &msgId, const string filename, int versionNum);
	virtual ~InvalidationMsg();

	MsgId msgId();
	void msgId(const MsgId& msgId);

	int versionNum();
	void versionNum(const int& versionnum);

	string filename();
	void filename(const string& filename);

	string toString() const;
	Message& assign(string sMessage);

	static string getFormat();

private:
	MsgId _msgId;
	string _filename;
	string _versionnum;
};

#endif /* INVALIDATIONMSG_H_ */
