/*
 * Worker.h
 *
 *  Created on: Sep 15, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef WORKER_H_
#define WORKER_H_

#include <string>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 *
 */
class Worker {
public:
	Worker();
	virtual ~Worker();

	virtual string run(const char *buf) = 0;
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* WORKER_H_ */
