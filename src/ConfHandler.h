/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * ConfHandler.h
 *
 *  Created on: Aug 7, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#ifndef CONFIGHANDLER_H_
#define CONFIGHANDLER_H_

#include "ConfEntry.h"

#include <map>
#include <vector>
#include <string>
#include <sys/types.h>

#include "Const-impl.h"

using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

class ConfHandler {
public:
	typedef map<string, ConfEntry> MAP; //key: PORT,50000 AND value:ConfEntry(PORT,50000)
	typedef pair<string, ConfEntry> PAIR;
	typedef MAP::iterator MIT;
	typedef MAP::reverse_iterator MRIT;

	typedef vector<ConfEntry> VEC;
	typedef VEC::iterator VIT;
	typedef VEC::reverse_iterator VRIT;

	typedef map<string, VEC> NMAP; //key: localhost:50010 AND value:a vector of ConfEntry(localhost:50005)
	typedef pair<string, VEC> NPAIR;
	typedef NMAP::iterator NMIT;
	typedef NMAP::reverse_iterator NMRIT;

public:
	ConfHandler();
	virtual ~ConfHandler();

	static void initConf(string envConf);
	static void initConf(string envConf, string neighborconf);
	static string getPortFromConf();
	static string getProtocolFromConf();

	static string getIdxSrv();
	static uint getIdxPort();
	static string getIdxPort_str();

	static void print_map();

private:
	static void setEnvParameters(const string& envConf);
	static void setNeighborParameters(const string& neighborconf);
	static void pickEnvParameters();
	static void pickNeighborParameters();

	static void setParametersInternal(string configFile, MAP& configMap);

	static void clear_all();

public:
	static MAP EnvParameters;
	static MAP NeighborParameters;
	static NMAP NeighborMap;
	static VEC NeighborVec;

public:
	static string CONF_ENV;
	static string CONF_NEIGHBOR;

private:
	static bool BEEN_INIT;
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* CONFIGHANDLER_H_ */
