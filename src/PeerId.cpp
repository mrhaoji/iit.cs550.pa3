/*
 * PeerId.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "PeerId.h"

#include "Const-impl.h"

#include<stdio.h>
#include<string.h>

using namespace iit::cs550::pa;

PeerId::PeerId() :
		_peerIp(""), _peerPort("") {
}

PeerId::PeerId(const string &peerIp, const string &peerPort) :
		_peerIp(peerIp), _peerPort(peerPort) {
}

PeerId::~PeerId() {
}

string PeerId::peerIp() {

	return _peerIp;
}

void PeerId::peerIp(const string& peerIp) {

	_peerIp = peerIp;
}

string PeerId::peerPort() {

	return _peerPort;
}

void PeerId::peerPort(const string& peerPort) {

	_peerPort = peerPort;
}

string PeerId::toString() const {

	char buf[50];
	memset(buf, 0, sizeof(buf));
	int n = sprintf(buf, getFormat().c_str(), _peerIp.c_str(), _peerPort.c_str());

	string result(buf, 0, n);

	return result;
}

PeerId& PeerId::assign(string sPeerId) {

	const char* delimiter = ":";

	string remains = Const::trim(sPeerId);

	size_t found = remains.find(delimiter);

	if (found != string::npos) {

		peerIp(Const::trim(remains.substr(0, int(found))));
		peerPort(Const::trim(remains.substr(int(found) + 1)));
	}

	return *this;
}

string PeerId::getFormat() {

	return "%s:%s";
}
