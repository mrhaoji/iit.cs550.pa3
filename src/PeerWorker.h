/*
 * PeerWorker.h
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef PEERWORKER_H_
#define PEERWORKER_H_

#include <string>
#include "zpack.pb.h"
#include "novoht.h"
#include "Worker.h"
#include "map.h"

using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 *
 */
class PeerWorker: public Worker {
public:
	typedef vector<string> VEC;
	typedef vector<string>::const_iterator VIT;

	typedef map<string, string> MAP;
	typedef MAP::iterator MIT;

public:
	PeerWorker();
	virtual ~PeerWorker();

public:
	virtual string run(const char *buf);

private:
	string obtain_file(const ZPack &zpack);
	string upload_file(const ZPack &zpack);
	string delete_file(const ZPack &zpack);
	string search_file(const ZPack &zpack);
	string fwd_query(const ZPack &zpack);
	string invalidate_file(const ZPack &zpack);

	string search_file_from_neighbors(const ZPack &zpack);

	void put_msgid(const string &msgid, MAP &map);
	void del_msgid(const string &msgidm, MAP &map);
	bool saw_msgid(const string &msgid, MAP &map);
private:
	string get_addr();

public:
	static string HOST;
	static string PORT;

private:
	static NoVoHT *pmap;
	static MAP SMM; //SeenMsgMap
	static MAP FMM; //ForwardedMsgMap
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* PEERWORKER_H_ */
