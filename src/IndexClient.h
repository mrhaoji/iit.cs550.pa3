/*
 * IndexClient.h
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef INDEXCLIENT_H_
#define INDEXCLIENT_H_

#include "ProxyStubFactory.h"

#include <vector>
#include <string>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 *
 */
class IndexClient {
public:
	typedef vector<string> VEC;
	typedef vector<string>::const_iterator IT;

public:
	IndexClient();
	virtual ~IndexClient();

	int init(const string &envConf);

	int register_peer(const string &peer, const VEC &filelist);
	int unregister_peer(const string &peer);
	/*filename: path removed*/
	int search_file(const string &filename, VEC &peerlist);
	int add_file(const string &peer, const VEC &filelist);
	int delete_file(const string &peer, const VEC &filelist);

	int teardown();

private:
	string sendrecv(const string &msg, string &result);
	int shared_op(const VEC &filelist, const string &op, string &result);
	int shared_op(const string &peer, const VEC &filelist, const string &op,
			string &result);

private:
	ProtoProxy *_proxy;
	int _msg_maxsize;
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* INDEXCLIENT_H_ */
