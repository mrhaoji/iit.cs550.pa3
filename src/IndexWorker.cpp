/*
 * IndexWorker.cpp
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#include <iostream>
#include "Const.h"
#include "IndexWorker.h"
#include <set>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 * key: peerid
 * value: ZPack: peerid, host, port, filelist....
 * */
//NoVoHT* IndexWorker::pmap = new NoVoHT("peers.index.store", 100000, 10000, 0.7);
NoVoHT* IndexWorker::pmap = new NoVoHT("", 100000, 10000, 0.7);

IndexWorker::IndexWorker() {
}

IndexWorker::~IndexWorker() {
}

string IndexWorker::run(const char *buf) {

	string result;

	ZPack zpack;
	string str(buf);
	zpack.ParseFromString(str);

	if (zpack.opcode() == Const::PSC_OPC_REGPR) {

		result = register_peer(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_UNREGPR) {

		result = unregister_peer(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_SRCHFL) {

		result = search_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_ADDFL) {

		result = add_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_DELFL) {

		result = delete_file(zpack);
	} else if (zpack.opcode() == Const::PSC_OPC_GETPRS) {

		result = get_allpeers(zpack);

	} else {

		result = Const::PSC_REC_UOPC;
	}

	return result;
}

string IndexWorker::register_peer(const ZPack &zpack) {

	string result;

	string key = zpack.peerid();
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	int ret = pmap->put(key, zpack.SerializeAsString());

	if (ret != 0) {

		cerr << "DB Error: fail to insert: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

string IndexWorker::unregister_peer(const ZPack &zpack) {

	string result;

	string key = zpack.peerid();
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	int ret = pmap->remove(key);

	if (ret != 0) {

		cerr << "DB Error: fail to remove: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

/*
 * key: peerid
 * value: ZPack: peerid, host, port, filelist....
 * */
string IndexWorker::search_file(const ZPack &zpack) {

	ZPack rltpack;

	string filename_passedin = zpack.filelist(0);

	/*iterate NOVOHT*/
	key_iterator ki = pmap->keyIterator(); //todo: this may not work correctly

	/*iterate all peers to search that file*/
	while (ki.hasNext()) {

		string key = ki.next();
		string *result = pmap->get(key);

		ZPack zpack2;
		zpack2.ParseFromString(*result);

		/*iterate all files in one peer*/
		for (int i = 0; i < zpack2.filelist().size(); i++) {

			string filename = zpack2.filelist(i);

			if (filename == filename_passedin) {

				/*prpack: peerid, host, port*/
				ZPack prpack;
				prpack.set_peerid(zpack2.peerid());
				prpack.set_prhost(zpack2.prhost());
				prpack.set_prport(zpack2.prport());

				rltpack.add_peerlist(prpack.SerializeAsString());

				break;
			}
		}

	}

	string result = Const::PSC_REC_SUCC;
	result.append(rltpack.SerializeAsString());

	return result;
}

string IndexWorker::add_file(const ZPack &zpack) {

	string result;

	string key = zpack.peerid();
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	/*lookup files in that peer*/
	string *lkpresult = pmap->get(key);
	if (lkpresult == NULL) {

		cerr << "DB Error: lookup find nothing" << endl;

		result = Const::PSC_REC_NONEXISTKEY;

		return result;
	}

	ZPack zpack2;
	zpack2.ParseFromString(*lkpresult);

	/*backup files stored*/
	SET backup_filelist;
	for (int i = 0; i < zpack2.filelist().size(); i++) {

		backup_filelist.insert(zpack2.filelist(i));
	}

	/*merge files to that peer, remove repeated ones*/
	for (int i = 0; i < zpack.filelist().size(); i++) {

		IT it = backup_filelist.find(zpack.filelist(i));
		if (it != backup_filelist.end())
			continue;

		backup_filelist.insert(zpack.filelist(i));
	}

	/*copy merges back to zpack2*/
	zpack2.clear_filelist();
	for (IT it = backup_filelist.begin(); it != backup_filelist.end(); ++it) {

		zpack2.add_filelist(*it);
	}

	/*store all files for that peer*/
	int ret = pmap->put(key, zpack2.SerializeAsString());

	if (ret != 0) {

		cerr << "DB Error: fail to insert: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

string IndexWorker::get_allpeers(const ZPack &zpack) {

	ZPack rltpack;

	/*iterate NOVOHT*/
	key_iterator ki = pmap->keyIterator(); //todo: this may not work correctly

	/*iterate all peers to search that file*/
	while (ki.hasNext()) {

		string key = ki.next();
		string *result = pmap->get(key);

		ZPack zpack2;
		zpack2.ParseFromString(*result);

		ZPack prpack;
		prpack.set_peerid(zpack2.peerid());
		prpack.set_prhost(zpack2.prhost());
		prpack.set_prport(zpack2.prport());

		rltpack.add_peerlist(prpack.SerializeAsString());
	}

	string result = Const::PSC_REC_SUCC;
	result.append(rltpack.SerializeAsString());

	return result;
}

string IndexWorker::delete_file(const ZPack &zpack) {

	string result;

	string key = zpack.peerid();
	if (key.empty())
		return Const::PSC_REC_EMPTYKEY; //-1

	/*lookup files in that peer*/
	string *lkpresult = pmap->get(key);
	if (lkpresult == NULL) {

		cerr << "DB Error: lookup find nothing" << endl;

		result = Const::PSC_REC_NONEXISTKEY;

		return result;
	}

	ZPack zpack2;
	zpack2.ParseFromString(*lkpresult);

	/*backup files stored*/
	SET backup_filelist;
	for (int i = 0; i < zpack2.filelist().size(); i++) {

		backup_filelist.insert(zpack2.filelist(i));
	}

	/*delete files from backup*/
	for (int j = 0; j < zpack.filelist().size(); j++) {

		string filename = zpack.filelist(j);

		IT it = backup_filelist.find(filename);
		if (it != backup_filelist.end())
			backup_filelist.erase(it);
	}

	/*move backup filelist changed to zpack2.filelist*/
	zpack2.clear_filelist();
	for (IT it = backup_filelist.begin(); it != backup_filelist.end(); it++) {

		zpack2.add_filelist(*it);
	}

	/*store all files for that peer*/
	int ret = pmap->put(key, zpack2.SerializeAsString());

	if (ret != 0) {

		cerr << "DB Error: fail to insert: rcode = " << ret << endl;
		result = Const::PSC_REC_NONEXISTKEY; //-92

	} else {

		result = Const::PSC_REC_SUCC; //0, succeed.
	}

	return result;
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
