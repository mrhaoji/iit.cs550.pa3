/*
 * HitQueryMsg.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef HITQUERYMSG_H_
#define HITQUERYMSG_H_

#include <map>
#include <string>
using namespace std;

#include "map.h"
#include "Message.h"
/*
 *
 */
class HitQueryMsg: public Message {

public:
	HitQueryMsg();
	HitQueryMsg(const MsgId &msgId, int ttl, const string filename);
	virtual ~HitQueryMsg();

	string toString() const;
	HitQueryMsg& assign(string sHitQueryMsg);

//	static string getFormat();

private:
	HitQueryMap _hqMap;
};

#endif /* HITQUERYMSG_H_ */
