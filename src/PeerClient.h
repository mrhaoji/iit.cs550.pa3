/*
 * PeerClient.h
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef PEERCLIENT_H_
#define PEERCLIENT_H_

#include "ProxyStubFactory.h"
#include "zpack.pb.h"

#include <string>
#include <vector>

using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 *
 */
class PeerClient {
public:
	typedef vector<string> VEC;
	typedef vector<string>::const_iterator IT;

public:
	PeerClient();
	virtual ~PeerClient();

	int init(const string &envConf);
	int init(const string &envConf, const string &neighborConf);

	int lookup_file(const string &filename, VEC &peerlist);
	int lookup_file(const string &peeraddr, const string &filename,
			VEC &peerlist);
	int obtain_file(const string &filename, const string &filename_down);
	int obtain_file(const string &peeraddr, const string &filename,
			const string &filename_down);
	int upload_file(const string &filename);
	int delete_file(const string &filename);

	int fwd_lookup(const string &addr, const ZPack& zpack, VEC &peerlist);

	int teardown();

	int invalidate_file(const string &filename);

private:
	string get_peer_for_file(const string &filename);
	int get_allpeers(VEC &peerlist);
	string sendrecv(const string &msg, string &result);
	string remove_path(const string &filename);

	int uploadfile_internal(const string &host, const uint &port,
			const string &filename, const char *filecontent);

	string get_random_peer_addr();

private:
	string envConf;
	string neighborConf;
	ProtoProxy *_proxy;
	int _msg_maxsize;
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* PEERCLIENT_H_ */
