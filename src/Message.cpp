/*
 * Message.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include "Message.h"
#include "MsgId.h"
#include "PeerId.h"

#include "Const-impl.h"
#include "StrTokenizer.h"

#include<stdio.h>
#include<string.h>

using namespace iit::cs550::pa;

Message::Message() :
		_msgId(), _ttl(0), _filename("") {
}

Message::Message(const MsgId &msgId, int ttl, const string filename) :
		_msgId(msgId), _ttl(ttl), _filename(filename) {

}

Message::~Message() {
}

MsgId Message::msgId() {

	return _msgId;
}

void Message::msgId(const MsgId& msgId) {

	_msgId = msgId;
}

int Message::ttl() {
	return _ttl;
}

void Message::ttl(const int& ttl) {

	_ttl = ttl;
}

string Message::filename() {

	return _filename;
}

void Message::filename(const string& filename) {

	_filename = filename;
}

string Message::toString() const {

	char buf[50];
	memset(buf, 0, sizeof(buf));
	int n = sprintf(buf, getFormat().c_str(), _msgId.toString().c_str(), _filename.c_str(), _ttl);

	string result(buf, 0, n);

	return result;
}

Message& Message::assign(string sMessage) {

		StrTokenizer strtok(sMessage, ",");

		_msgId.assign(strtok.next_token());
		_filename = strtok.next_token();
		_ttl = atoi(strtok.next_token().c_str());

		return *this;
}

string Message::getFormat() {

	return "%d,%s,%d";
}
