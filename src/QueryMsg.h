/*
 * QueryMsg.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef QUERYMSG_H_
#define QUERYMSG_H_

#include <string>
using namespace std;
#include "MsgId.h"
#include "Message.h"

/*
 *
 */
class QueryMsg: public Message {
public:
	QueryMsg();
	QueryMsg(const MsgId &msgId, int ttl, const string filename);
	virtual ~QueryMsg();

	string toString() const;
	QueryMsg& assign(string sQueryMsg);

//	static string getFormat();
};

#endif /* QUERYMSG_H_ */
