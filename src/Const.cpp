/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * Const.cpp
 *
 *  Created on: Jul 23, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#include "Const-impl.h"

namespace iit {
namespace cs550 {
namespace pa {

const string Const::StringEmpty = "";

const string Const::LOCAL_HOST = "localhost";

const string Const::DIR_DELIM = "/";

const string Const::CONF_DELIMITERS = " ";

const int Const::ADDR_UUIDZERO = 0;

const int Const::PROTO_STREAM = 1;
const int Const::PROTO_UGRADM = 2;

const string Const::NC_ZHT_CAPACITY = "ZHT_CAPACITY";
const string Const::NC_FILECLIENT_PATH = "FILECLIENT_PATH";
const string Const::NC_FILESERVER_PATH = "FILESERVER_PATH";
const string Const::NC_FILESERVER_PORT = "FILESERVER_PORT";

const string Const::ZC_MAX_ZHT = "MAX_ZHT";
const string Const::ZC_NUM_REPLICAS = "NUM_REPLICAS";
const string Const::ZC_REPLICATION_TYPE = "REPLICATION_TYPE";
const string Const::ZC_HTDATA_PATH = "HTDATA_PATH";
const string Const::ZC_MIGSLP_TIME = "MIGSLP_TIME";
const string Const::ZC_PROTOCOL = "PROTOCOL";

const string Const::PROTO_NAME = "PROTOCOL";
const string Const::PROTO_PORT = "PORT";
const string Const::PROTO_VAL_TCP = "TCP";
const string Const::PROTO_VAL_UDP = "UDP";
const string Const::PROTO_VAL_UDT = "UDT";
const string Const::PROTO_VAL_MPI = "MPI";

const string Const::IDX_SRV = "IDX_SRV";
const string Const::IDX_PRT = "IDX_PRT";

const string Const::MSG_MAXSIZE = "MSG_MAXSIZE";

const string Const::PSC_OPC_REGPR = "001";
const string Const::PSC_OPC_UNREGPR = "002";
const string Const::PSC_OPC_SRCHFL = "003";
const string Const::PSC_OPC_ADDFL = "004";
const string Const::PSC_OPC_DELFL = "005";
const string Const::PSC_OPC_LKPFL = "006";
const string Const::PSC_OPC_OBTFL = "007";
const string Const::PSC_OPC_UPLFL = "008";
const string Const::PSC_OPC_GETPRS = "009";
const string Const::PSC_OPC_FWDQUERY = "010";
const string Const::PSC_OPC_INVFL = "011";

const string Const::PSC_OPC_OPR_CANCEL = "088";

const string Const::PSC_REC_EMPTYKEY = "-01";
const string Const::PSC_REC_CLTFAIL = "-02";
const string Const::PSC_REC_SRVFAIL = "-03";
const string Const::PSC_REC_SRVEXP = "-04";
const string Const::PSC_REC_NONEXISTKEY = "-92";
const string Const::PSC_REC_UOPC = "-98";
const string Const::PSC_REC_UNPR = "-99";
const string Const::PSC_REC_SUCC = "000";

const int Const::PSI_OPC_OPR_CANCEL = 88;
const int Const::PSI_REC_EMPTYKEY = -1;
const int Const::PSI_REC_CLTFAIL = -2;
const int Const::PSI_REC_SRVFAIL = -3;
const int Const::PSI_REC_SRVEXP = -4;
const int Const::PSI_REC_FLOPFAIL = -5;
const int Const::PSI_REC_FLNOTFND = -6;
const int Const::PSI_REC_NOPEERS = -7;
const int Const::PSI_REC_UOPC = -98;
const int Const::PSI_REC_UNPR = -99;
const int Const::PSI_REC_SUCC = 0;

Const::Const() {
}

Const::~Const() {
}

string Const::trim(const string& value) {

	string str = value;
	stringstream trimmer;
	trimmer << str;
	trimmer >> str;

	return str;
}

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
