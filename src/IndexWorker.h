/*
 * IndexWorker.h
 *
 *  Created on: Sep 14, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef INDEXWORKER_H_
#define INDEXWORKER_H_

#include <set>
#include <string>
#include "zpack.pb.h"
#include "novoht.h"
#include "Worker.h"

using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

/*
 *
 */
class IndexWorker: public Worker {
public:
	typedef set<string> SET;
	typedef SET::const_iterator IT;

public:
	IndexWorker();
	virtual ~IndexWorker();
public:
	virtual string run(const char *buf);

private:
	string register_peer(const ZPack &zpack);
	string unregister_peer(const ZPack &zpack);
	string search_file(const ZPack &zpack);
	string add_file(const ZPack &zpack);
	string delete_file(const ZPack &zpack);
	string get_allpeers(const ZPack &zpack);

private:
	static NoVoHT *pmap;
};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* INDEXWORKER_H_ */
