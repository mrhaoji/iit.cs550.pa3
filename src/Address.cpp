/*
 * Address.cpp
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#include <string.h>
#include "Address.h"

#include "Const-impl.h"

using namespace iit::cs550::pa;

Address::Address() {
}

Address::Address(const string& saddress) {

	assign(saddress);
}

Address::Address(const string &host, const string &port) :
		_host(host), _port(port) {

}

Address::~Address() {
}

string Address::host() {

	return _host;
}

void Address::host(const string &host) {

	_host = host;
}

string Address::port() {

	return _port;
}

void Address::port(const string &port) {

	_port = port;
}

string Address::toString() const {

	char buf[50];
	memset(buf, 0, sizeof(buf));
	int n = sprintf(buf, getFormat().c_str(), _host.c_str(), _port.c_str());

	string result(buf, 0, n);

	return result;
}

Address& Address::assign(string saddress) {

	const char* delimiter = ":";

	string remains = Const::trim(saddress);

	size_t found = remains.find(delimiter);

	if (found != string::npos) {

		host(Const::trim(remains.substr(0, int(found))));
		port(Const::trim(remains.substr(int(found) + 1)));
	}

	return *this;
}

string Address::getFormat() {

	return "%s:%s";
}
