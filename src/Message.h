/*
 * Message.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <string>
using namespace std;

#include "MsgId.h"

#include "Const-impl.h"

/*
 *
 */
class Message {
public:
	Message();
	Message(const MsgId &msgId, int ttl, const string filename);
	virtual ~Message();

//	MsgId get_msgId();
//	int get_ttl();
//	string get_filename();

	MsgId msgId();
	void msgId(const MsgId& msgId);

	int ttl();
	void ttl(const int& ttl);

	string filename();
	void filename(const string& filename);

	string toString() const;
	Message& assign(string sMessage);

	static string getFormat();

private:
	MsgId _msgId;
	int _ttl;
	string _filename;

};

#endif /* MESSAGE_H_ */
