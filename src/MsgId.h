/*
 * MsgId.h
 *
 *  Created on: Oct 5, 2013
 *      Author: Xiaobing Zhou
 */

#ifndef MSGID_H_
#define MSGID_H_

#include <string>
using namespace std;

#include "PeerId.h"

/*
 *
 */
class MsgId {
public:
	MsgId();
	MsgId(const string &seqNum, const PeerId &peerId);
	virtual ~MsgId();

//	string get_seqNum();
//	PeerId get_peerId();

	string seqNum();
	void seqNum(const string& seqNum);

	PeerId peerId();
	void peerId(const PeerId& peerId);

	string toString() const;
	MsgId& assign(string sMsgId);

	static string getFormat();

private:
	string _seqNum;
	PeerId _peerId;
};
#endif /* MSGID_H_ */
