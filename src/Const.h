/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * Const.h
 *
 *  Created on: Jul 23, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include <map>
#include <string>
#include <stdint.h>
using namespace std;

namespace iit {
namespace cs550 {
namespace pa {

class Const {

public:
	Const();
	virtual ~Const();

public:
	template<class TYPE> static int toInt(const TYPE& ele);
	template<class TYPE> static uint64_t toUInt64(const TYPE& ele);
	template<class TYPE> static string toString(const TYPE& ele);
	template<class TYPE1, class TYPE2> static string concat(const TYPE1& ele1,
			const TYPE2& ele2);
	template<class TYPE1, class TYPE2> static string concat(const TYPE1& ele1,
			const string& delimiter, const TYPE2& ele2);

	static string trim(const string& value);

public:
	static const string StringEmpty;

	static const string LOCAL_HOST;

	static const string DIR_DELIM;

	static const string CONF_DELIMITERS; //delimiters used in config file

	static const int ADDR_UUIDZERO; //used to remember if the address is changed.

	/*
	 * PROTO_: protocol
	 */
	static const int PROTO_STREAM;
	static const int PROTO_UGRADM;

	/*
	 * NC_: node config
	 */
	static const string NC_ZHT_CAPACITY;
	static const string NC_FILECLIENT_PATH;
	static const string NC_FILESERVER_PATH;
	static const string NC_FILESERVER_PORT;

	/*
	 * ZC_: ZHT config
	 */
	static const string ZC_MAX_ZHT;
	static const string ZC_NUM_REPLICAS;
	static const string ZC_REPLICATION_TYPE;
	static const string ZC_HTDATA_PATH;
	static const string ZC_MIGSLP_TIME;
	static const string ZC_PROTOCOL;

	/*
	 * PROTO_: protocol
	 */
	static const string PROTO_NAME;
	static const string PROTO_PORT;
	static const string PROTO_VAL_TCP;
	static const string PROTO_VAL_UDP;
	static const string PROTO_VAL_UDT;
	static const string PROTO_VAL_MPI;

	/*
	 * IDX_: index
	 */
	static const string IDX_SRV;
	static const string IDX_PRT;

	/*
	 * MSG_: message
	 */
	static const string MSG_MAXSIZE;

	/*
	 * PSC_: peer(service) chars
	 * OPC_: operation code
	 * REC_: return code
	 * UNPR_: unprocessed
	 * UOPC_: unrecognized operation code
	 * SUCC_: succeeded
	 */
	static const string PSC_OPC_REGPR; //register_peer
	static const string PSC_OPC_UNREGPR; //unregister_peer
	static const string PSC_OPC_SRCHFL; //search_file
	static const string PSC_OPC_ADDFL; //add_file
	static const string PSC_OPC_DELFL; //delete_file

	static const string PSC_OPC_LKPFL; //look_file
	static const string PSC_OPC_OBTFL; //obtain_file
	static const string PSC_OPC_UPLFL; //upload_file
	static const string PSC_OPC_GETPRS; //get peers
	static const string PSC_OPC_FWDQUERY; //forward query
	static const string PSC_OPC_INVFL; //invilate file

	static const string PSC_OPC_OPR_CANCEL; //cancle an operation

	static const string PSC_REC_EMPTYKEY; //empty key
	static const string PSC_REC_CLTFAIL; //operation failed in client-side
	static const string PSC_REC_SRVFAIL; //operation failed in server-side
	static const string PSC_REC_SRVEXP; //operation failed
	static const string PSC_REC_NONEXISTKEY;
	static const string PSC_REC_UOPC; //unrecognized operation code
	static const string PSC_REC_UNPR; //unprocessed
	static const string PSC_REC_SUCC; //succeeded

	static const int PSI_OPC_OPR_CANCEL; //cancel an operation
	static const int PSI_REC_EMPTYKEY; //empty key
	static const int PSI_REC_CLTFAIL; //operation failed in client-side
	static const int PSI_REC_SRVFAIL; //operation failed in server-side
	static const int PSI_REC_SRVEXP; //server excpetion
	static const int PSI_REC_FLOPFAIL; //file open failed
	static const int PSI_REC_FLNOTFND; //file not found
	static const int PSI_REC_NOPEERS; //no peers found
	static const int PSI_REC_UOPC; //unrecognized operation code
	static const int PSI_REC_UNPR; //unprocessed
	static const int PSI_REC_SUCC; //succeeded

};

} /* namespace pa */
} /* namespace cs550 */
} /* namespace iit */
#endif /* CONSTANTS_H_ */
